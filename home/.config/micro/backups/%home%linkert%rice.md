* **Bar:** I don't do a bar no more, too much hassle when docking the laptop and such - I now keep it to a notification via keyboard shortcut.
* **Blunt scripts that makes things work:**
  * ```wal{dark,light}``` - switches between light and dark theme, sets picom shadow color, reloads whatever needs reloading.
  * ```sunwal``` - checks if sun has risen or night has fallen and switches theme accordingly - have it run every 10 minutes via cron.
  * ```barba{music,web,fm,gemini}``` - each one of the different "barba"-scripts utilize wmctrl to check if x application is running. For instance ```barbamusic``` checks if ```cmus``` is running. If not: run it - if yes: focus it. This way I don't end up with multiple instances of different applications.
  * ```barbastatus``` - "status notification" that sends status for time, battery, volume, available updates, rss (does not work), if temperature is above 75C or something it also displays temperature. Under construction - I quite recently got some momentum with shell scripting ;)
  * ```walrestore``` - restores pywal theme and sets background at startup.
* **Compositor:** picom-ibhagwan
* **Fetch:** pfetch - sofa king retarded say funny thing
* **Fonts:** [Iosevka](https://github.com/be5invis/Iosevka/) Term Medium 10, Iosevka Aile 10, [Cozette](https://github.com/slavfox/Cozette)Vector 9
* **Shell:** fish
* **Terminal emulator:** Alacritty - gosh darn it please support ligatures, my Iosevka needs to be let free.
* **Things utilized:** qutebrowser, amfora, nnn, cmus, pavucontrol, Aseprite, pfetch, dunst, pywal, sunwait, ImageMagick
* **Wallpaper:** The wallpaper is a play on the macOS Big Sur wallpaper where I traced the gradients in Inkscape and then set the color values to pywal variables in order for it to change with the currently generated color theme. I call it [Big Void](https://gitlab.com/linkert/bigvoid)
* **WM:** spectrwm <3

[Snapshot of relevant dotfiles]()