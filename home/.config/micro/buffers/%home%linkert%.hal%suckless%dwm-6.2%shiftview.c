I’SerializedBuffer’ EventHandler’ Cursor’ ModTime’   8’EventHandler’ 	UndoStack’ 	RedoStack’   '’TEStack’ Top’ Size   *’Element’ Value’ Next’   B’	TextEvent’ C’ 	EventType Deltas’ Time’   Z’Cursor’ Loc’ LastVisualX CurSelection’ OrigSelection’ Num   ’Loc’ X Y   ’[2]buffer.Loc’ ’  ’[]buffer.Delta’ ’  0’Delta’ Text
 Start’ End’   ’Time’   ž’      ž=/** Function to shift the current view to the left/right
 *
 * @param: "arg->i" stores the number of tags to shift right (positive value)
 *          or left (negative value)
 */
void
shiftview(const Arg *arg) {
	Arg shifted;

	if(arg->i > 0) // left circular shift
		shifted.ui = (selmon->tagset[selmon->seltags] << arg->i)
		   | (selmon->tagset[selmon->seltags] >> (LENGTH(tags) - arg->i));

	else // right circular shift
		shifted.ui = selmon->tagset[selmon->seltags] >> (- arg->i)
		   | selmon->tagset[selmon->seltags] << (LENGTH(tags) + arg->i);

	view(&shifted);
}
 &     Ö<\6.oł x     $    Ö<\/ x 